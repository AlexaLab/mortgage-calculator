﻿namespace Срапотека
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Nomber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Month = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Plat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Plat_Ipot = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Proc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Dolg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.textBox_Ipoteka = new System.Windows.Forms.TextBox();
            this.textBox_proc_vznos = new System.Windows.Forms.TextBox();
            this.textBox_vznos = new System.Windows.Forms.TextBox();
            this.numericUpDown_srok = new System.Windows.Forms.NumericUpDown();
            this.textBox_proc_stavka = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox_plata = new System.Windows.Forms.TextBox();
            this.textBox_pereplata = new System.Windows.Forms.TextBox();
            this.textBox_itog = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox_dolg = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_srok)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Nomber,
            this.Month,
            this.Plat,
            this.Plat_Ipot,
            this.Proc,
            this.Dolg});
            this.dataGridView1.Location = new System.Drawing.Point(533, 37);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(635, 650);
            this.dataGridView1.TabIndex = 0;
            // 
            // Nomber
            // 
            this.Nomber.HeaderText = "№";
            this.Nomber.Name = "Nomber";
            // 
            // Month
            // 
            this.Month.HeaderText = "Месяц";
            this.Month.Name = "Month";
            // 
            // Plat
            // 
            this.Plat.HeaderText = "Ежемесячный платёж";
            this.Plat.Name = "Plat";
            // 
            // Plat_Ipot
            // 
            this.Plat_Ipot.HeaderText = "Оплата долга";
            this.Plat_Ipot.Name = "Plat_Ipot";
            // 
            // Proc
            // 
            this.Proc.HeaderText = "Оплата процентов";
            this.Proc.Name = "Proc";
            // 
            // Dolg
            // 
            this.Dolg.HeaderText = "Основной долг";
            this.Dolg.Name = "Dolg";
            // 
            // textBox_Ipoteka
            // 
            this.textBox_Ipoteka.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox_Ipoteka.Location = new System.Drawing.Point(291, 185);
            this.textBox_Ipoteka.Name = "textBox_Ipoteka";
            this.textBox_Ipoteka.Size = new System.Drawing.Size(200, 23);
            this.textBox_Ipoteka.TabIndex = 1;
            this.textBox_Ipoteka.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Ipoteka_KeyPress);
            // 
            // textBox_proc_vznos
            // 
            this.textBox_proc_vznos.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox_proc_vznos.Location = new System.Drawing.Point(291, 223);
            this.textBox_proc_vznos.Name = "textBox_proc_vznos";
            this.textBox_proc_vznos.Size = new System.Drawing.Size(200, 23);
            this.textBox_proc_vznos.TabIndex = 2;
            this.textBox_proc_vznos.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.proc_vznos_KeyPress);
            // 
            // textBox_vznos
            // 
            this.textBox_vznos.Enabled = false;
            this.textBox_vznos.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox_vznos.Location = new System.Drawing.Point(291, 265);
            this.textBox_vznos.Name = "textBox_vznos";
            this.textBox_vznos.Size = new System.Drawing.Size(200, 23);
            this.textBox_vznos.TabIndex = 3;
            // 
            // numericUpDown_srok
            // 
            this.numericUpDown_srok.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numericUpDown_srok.Location = new System.Drawing.Point(291, 397);
            this.numericUpDown_srok.Name = "numericUpDown_srok";
            this.numericUpDown_srok.Size = new System.Drawing.Size(200, 23);
            this.numericUpDown_srok.TabIndex = 4;
            // 
            // textBox_proc_stavka
            // 
            this.textBox_proc_stavka.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox_proc_stavka.Location = new System.Drawing.Point(291, 351);
            this.textBox_proc_stavka.Name = "textBox_proc_stavka";
            this.textBox_proc_stavka.Size = new System.Drawing.Size(200, 23);
            this.textBox_proc_stavka.TabIndex = 5;
            this.textBox_proc_stavka.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.proc_stavka_KeyPress);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radioButton2);
            this.panel1.Controls.Add(this.radioButton1);
            this.panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.panel1.Location = new System.Drawing.Point(35, 37);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(214, 100);
            this.panel1.TabIndex = 6;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(17, 58);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(178, 21);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Диффиренцированная";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(17, 20);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(160, 21);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Аннуитетная форма";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(36, 188);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(185, 17);
            this.label1.TabIndex = 7;
            this.label1.Text = "Размер ипотеки (в рублях)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(36, 226);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(233, 17);
            this.label2.TabIndex = 8;
            this.label2.Text = "Процент первоначального взноса";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(36, 268);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(163, 17);
            this.label3.TabIndex = 9;
            this.label3.Text = "Первоначальный взнос";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(36, 354);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(194, 17);
            this.label4.TabIndex = 10;
            this.label4.Text = "Годовая процентная ставка";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(36, 399);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(197, 17);
            this.label5.TabIndex = 11;
            this.label5.Text = "Срок кредитования (в годах)";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(291, 75);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 13;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Location = new System.Drawing.Point(39, 625);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(200, 62);
            this.button1.TabIndex = 14;
            this.button1.Text = "Рассчитать";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox_plata
            // 
            this.textBox_plata.Enabled = false;
            this.textBox_plata.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox_plata.Location = new System.Drawing.Point(291, 556);
            this.textBox_plata.Name = "textBox_plata";
            this.textBox_plata.Size = new System.Drawing.Size(200, 23);
            this.textBox_plata.TabIndex = 17;
            // 
            // textBox_pereplata
            // 
            this.textBox_pereplata.Enabled = false;
            this.textBox_pereplata.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox_pereplata.Location = new System.Drawing.Point(291, 511);
            this.textBox_pereplata.Name = "textBox_pereplata";
            this.textBox_pereplata.Size = new System.Drawing.Size(200, 23);
            this.textBox_pereplata.TabIndex = 16;
            // 
            // textBox_itog
            // 
            this.textBox_itog.Enabled = false;
            this.textBox_itog.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox_itog.Location = new System.Drawing.Point(291, 468);
            this.textBox_itog.Name = "textBox_itog";
            this.textBox_itog.Size = new System.Drawing.Size(200, 23);
            this.textBox_itog.TabIndex = 15;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(36, 559);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(212, 17);
            this.label6.TabIndex = 20;
            this.label6.Text = "Средний ежемесячный платёж";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(36, 514);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(81, 17);
            this.label7.TabIndex = 19;
            this.label7.Text = "Переплата";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(36, 471);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(141, 17);
            this.label8.TabIndex = 18;
            this.label8.Text = "Итоговая стоимость";
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button2.Location = new System.Drawing.Point(291, 625);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(200, 62);
            this.button2.TabIndex = 21;
            this.button2.Text = "Очистить";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(36, 310);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(106, 17);
            this.label9.TabIndex = 23;
            this.label9.Text = "Основной долг";
            // 
            // textBox_dolg
            // 
            this.textBox_dolg.Enabled = false;
            this.textBox_dolg.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox_dolg.Location = new System.Drawing.Point(291, 307);
            this.textBox_dolg.Name = "textBox_dolg";
            this.textBox_dolg.Size = new System.Drawing.Size(200, 23);
            this.textBox_dolg.TabIndex = 22;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(288, 37);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(108, 17);
            this.label10.TabIndex = 24;
            this.label10.Text = "Начало вклада";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1207, 718);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.textBox_dolg);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.textBox_plata);
            this.Controls.Add(this.textBox_pereplata);
            this.Controls.Add(this.textBox_itog);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.textBox_proc_stavka);
            this.Controls.Add(this.numericUpDown_srok);
            this.Controls.Add(this.textBox_vznos);
            this.Controls.Add(this.textBox_proc_vznos);
            this.Controls.Add(this.textBox_Ipoteka);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Form1";
            this.Text = "Ипотека";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_srok)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nomber;
        private System.Windows.Forms.DataGridViewTextBoxColumn Month;
        private System.Windows.Forms.DataGridViewTextBoxColumn Plat;
        private System.Windows.Forms.DataGridViewTextBoxColumn Plat_Ipot;
        private System.Windows.Forms.DataGridViewTextBoxColumn Proc;
        private System.Windows.Forms.DataGridViewTextBoxColumn Dolg;
        private System.Windows.Forms.TextBox textBox_Ipoteka;
        private System.Windows.Forms.TextBox textBox_proc_vznos;
        private System.Windows.Forms.TextBox textBox_vznos;
        private System.Windows.Forms.NumericUpDown numericUpDown_srok;
        private System.Windows.Forms.TextBox textBox_proc_stavka;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox_plata;
        private System.Windows.Forms.TextBox textBox_pereplata;
        private System.Windows.Forms.TextBox textBox_itog;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox_dolg;
        private System.Windows.Forms.Label label10;
    }
}

