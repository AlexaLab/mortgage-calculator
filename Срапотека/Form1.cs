﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Срапотека
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Ipoteka_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != 8)
                e.Handled = true;
        }

        private void proc_vznos_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != 8)
                e.Handled = true;
        }

        private void proc_stavka_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != 8 && !(e.KeyChar == ','))
                e.Handled = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DateTime date = dateTimePicker1.Value;

            double k,                //коэффициент аннуитета
                rasm_ipot,           //размер ипотеки
                proc_perv_vznos,     //процент первоначального взноса
                perv_vznos,          //первоначальный взнос
                proc_s,              //процентная ставка
                srok,                //срок кредитования
                itog = 0,            //итоговая стоимость
                perepl,              //переплата
                plat,                //ежемесячный платёж
                p,                   //месячная процентная ставка
                dolg,                //долг по кредиту без процентов банку
                oplata_dolg,         //оплата долга
                oplata_proc,         //оплата процентов
                ost_dolg,            //остаток долга в периоде
                sr_plat,             //средний ежемесячный платёж
                n = 0,               //число прошедших периодов
                number_month,        //номер месяца
                day = 0,             //количество дней в месяце
                day_year;            //количество дней а году

            string month = "";       //Месяц


            if (textBox_Ipoteka.Text == "" || textBox_proc_stavka.Text == "" || textBox_proc_vznos.Text == "" || numericUpDown_srok.Value == 0)
            {
                MessageBox.Show("Вы заполнили не все поля!");
                return;
            }


            rasm_ipot = Convert.ToDouble(textBox_Ipoteka.Text);
            proc_perv_vznos = Convert.ToDouble(textBox_proc_vznos.Text);
            proc_s = Convert.ToDouble(textBox_proc_stavka.Text);
            srok = Convert.ToDouble(numericUpDown_srok.Value);
            dataGridView1.RowCount = (int)srok * 12;

            perv_vznos = rasm_ipot * proc_perv_vznos / 100;
            textBox_vznos.Text = Convert.ToString(perv_vznos);
            dolg = rasm_ipot - perv_vznos;
            textBox_dolg.Text = Convert.ToString(dolg);
            p = proc_s / 12 / 100;
            ost_dolg = dolg;

            if (radioButton1.Checked == true)
            {
                k = (p * Math.Pow((1 + p), srok * 12)) / (Math.Pow((1 + p), srok * 12) - 1);
                plat = k * dolg;
                textBox_plata.Text = Convert.ToString(Math.Round(plat, 2));

                for (int i = 0; i < srok * 12; i++)
                {
                    number_month = date.Month;
                    switch (number_month)
                    {
                        case 1:
                            month = "Январь"; day = 31; break;
                        case 2:
                            month = "Февраль";
                            if (DateTime.IsLeapYear(date.Year) == true)  day = 29;
                            else day = 28;
                            break;
                        case 3:
                            month = "Март"; day = 31; break;
                        case 4:
                            month = "Апрель"; day = 30; break;
                        case 5:
                            month = "Май"; day = 31; break;
                        case 6:
                            month = "Июнь"; day = 30; break;
                        case 7:
                            month = "Июль"; day = 31; break;
                        case 8:
                            month = "Август"; day = 31; break;
                        case 9:
                            month = "Сентябрь"; day = 30; break;
                        case 10:
                            month = "Октябрь"; day = 31; break;
                        case 11:
                            month = "Ноябрь"; day = 30; break;
                        case 12:
                            month = "Декабрь"; day = 31; break;
                    }
                    if (DateTime.IsLeapYear(date.Year) == true) day_year = 366;
                    else day_year = 365;
                    dataGridView1[0, i].Value = Convert.ToString(i + 1);
                    dataGridView1[1, i].Value = month;
                    date = date.AddMonths(1);

                    oplata_proc = (ost_dolg * (proc_s/100) * day) / day_year;
                    dataGridView1[4, i].Value = Convert.ToString(Math.Round(oplata_proc, 2));
                    oplata_dolg = plat - oplata_proc;
                    dataGridView1[3, i].Value = Convert.ToString(Math.Round(oplata_dolg, 2));
                    ost_dolg -= oplata_dolg;
                    dataGridView1[5, i].Value = Convert.ToString(Math.Round(ost_dolg, 2));
                    dataGridView1[2, i].Value = Convert.ToString(Math.Round(plat, 2));
                }
                itog = plat * srok * 12;              
            }
            else if (radioButton2.Checked == true)
            {
                for (int i = 0; i < srok * 12; i++)
                {
                    n++; 
                    oplata_dolg = dolg / (srok * 12);
                    dataGridView1[3, i].Value = Convert.ToString(Math.Round(oplata_dolg, 2));
                    oplata_proc = ost_dolg * p;
                    dataGridView1[4, i].Value = Convert.ToString(Math.Round(oplata_proc, 2));
                    ost_dolg = dolg - (oplata_dolg * n);
                    dataGridView1[5, i].Value = Convert.ToString(Math.Round(ost_dolg, 2));
                    plat = oplata_dolg + oplata_proc;
                    dataGridView1[2, i].Value = Convert.ToString(Math.Round(plat, 2));
                    dataGridView1[0, i].Value = Convert.ToString(i+1);
                    itog += plat;

                    number_month = date.Month;
                    switch (number_month)
                    {
                        case 1:                            month = "Январь"; break;
                        case 2:                            month = "Февраль"; break;
                        case 3:                            month = "Март"; break;
                        case 4:                            month = "Апрель"; break;
                        case 5:                            month = "Май"; break;
                        case 6:                            month = "Июнь"; break;
                        case 7:                            month = "Июль"; break;
                        case 8:                            month = "Август"; break;
                        case 9:                            month = "Сентябрь"; break;
                        case 10:                           month = "Октябрь"; break;
                        case 11:                           month = "Ноябрь"; break;
                        case 12:                           month = "Декабрь"; break;
                    }
                    dataGridView1[1, i].Value = month;
                    date = date.AddMonths(1);
                }
            }
            else
            {
                MessageBox.Show("Вы не выбрали форму оплаты!");
            }

            sr_plat = itog / (srok * 12);
            textBox_plata.Text = Math.Round(sr_plat, 2).ToString();
            perepl = itog - (rasm_ipot - perv_vznos);
            textBox_pereplata.Text = Math.Round(perepl, 2).ToString();
            textBox_itog.Text = Math.Round(itog, 2).ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox_vznos.Text = "";
            textBox_proc_vznos.Text = "";
            textBox_proc_stavka.Text = "";
            textBox_plata.Text = "";
            textBox_pereplata.Text = "";
            textBox_itog.Text = "";
            textBox_Ipoteka.Text = "";
            textBox_dolg.Text = "";
            numericUpDown_srok.Value = 0;
            dataGridView1.Rows.Clear();
        }
    }
}
